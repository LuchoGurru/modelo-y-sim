package Eventos;

import Eventos.Queue;
import Eventos.Servidor;
import Eventos.Fel;
import Eventos.Item;

public class EventoSalida extends Evento {
	/**
         * Crea un evento de tipo 1 el cual finalizara con el servicio
         * Calcula el tiempo de espera en cola y el tiempo de transito del item.
         * @param tiempo tiempo del evento
         * @param ultimo  item del evento
         */
   public EventoSalida(int tiempo,Item item){
	   super(1, tiempo, item);
	   Item.setTiempoEsperaCola(tiempo - item.getTiempoDuracionServicio(), item.getTiempoArribo());
	   Item.setTiempoTransito(tiempo, item.getTiempoArribo());
   }
   /**
    * Este metodo, en el caso de que haya elementos en la cola inserta en la fel un nuevo evento de salida con otro tiempo generado
    * si no hay cola marca el estado del servidor como desocupado
    * @param servidor
    * @param cola 
    */
   public void planificarEvento(Servidor servidor,Queue cola) {
	   if(cola.HayCola()) {
		   int ts = GeneradorTiempos.getTiempoDuracionServicio();
		   Item aux = cola.suprimirCola();
		   aux.setTiempoDuracionServicio(ts);
		   Fel.getFel().insertarFel(new EventoSalida(this.getTiempo()+ts, aux));
	   }
	   else {
		   servidor.setEstado(false);   
		   servidor.setTiempoInicioOcio(this.getTiempo());
	   }  
   }   
}
