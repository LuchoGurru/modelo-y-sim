package Eventos;

public class EventoArribo extends Evento{    
	
        /**
         * Crea un evento de tipo arribo, setea el tipo de evento en 0 por defecto y el tiempo de arribo del item.
         * @param tiempo
         * @param item 
         */
	public EventoArribo(int tiempo,Item item){
            super(0, tiempo, item);
            item.setTiempoArribo(tiempo);
        } 
	/**
         * Este metodo chequea el estado del servidor, si esta ocupado, mete el item del evento en la cola
         * si esta desocupado, marca el servidor como ocupado, e inserta en la fel su propio evento de salida.
         * al final del metodo inserta un nuevo evento de arribo.
         * @param servidor servidor a ocupar si esta desocupado
         * @param cola si el servidor esta desocupado suprime el primer item de la cola. 
         */
    public void planificarEvento(Servidor servidor,Queue cola){       
    	if(servidor.isEstado()) { // si es verdadero, esta ocupado
    		cola.insertarCola(this.getItem());
    	}
    	else {
    		servidor.setEstado(true);
    		int ts = GeneradorTiempos.getTiempoDuracionServicio();
    		this.getItem().setTiempoDuracionServicio(ts);
    		Fel.getFel().insertarFel(new EventoSalida(this.getTiempo() + ts, this.getItem()));
                servidor.setTiempoOcioso(this.getTiempo() - servidor.getTiempoInicioOcio());
    	}
    	Fel.getFel().insertarFel(new EventoArribo(this.getTiempo() + GeneradorTiempos.getTiempoEntreArribos(), new Item()));
    }
}
