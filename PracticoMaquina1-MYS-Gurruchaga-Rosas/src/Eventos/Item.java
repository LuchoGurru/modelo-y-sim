package Eventos;

public class Item  {
	private int numero;
	private int tiempoArribo;
	private int tiempoDuracionServicio;
	private static int tiempoEsperaCola=0;
	private static int tiempoTransito=0;
        // Dado que la FEL siempre tendra un evento arribo fuera del evento de fin de simulacion. 
        // Y nosotros al crear un Evento de Arribo por defecto creamos un item y por lo tanto la cantidad de items crece
        // Siempre tendremos un item por fuera de la simulacio y nuestro contador tendra un item de mas
        // Como tampoco contamos el item de fin simulacion en total tendriamos 2 items de mas.
        // Por lo que iniciamos por defecto la cantidad de items en -2
	private static int cantidadItems=-2;
	
	public Item(){
		this.tiempoArribo=0;
		this.tiempoDuracionServicio=0;
		cantidadItems++;
		this.numero=cantidadItems;
	}
        
	/**
	 * @return Returns the cantidadItems.
	 */
	public static int getCantidadItems() {
		return cantidadItems;
	}
		
	/**
	 * @return Returns the tiempoEsperaCola.
	 */
	public static int getTiempoEsperaCola() {
		return tiempoEsperaCola;
	}
	
        /**
	 * @param tiempoEsperaCola The tiempoEsperaCola to set.
	 */
	public static void setTiempoEsperaCola(int tiempoActual, int tiempoArribo) {
		tiempoEsperaCola += tiempoActual - tiempoArribo;
	}
	
    /**
	 * @return Returns the tiempoTransito.
	 */
	public static int getTiempoTransito() {
		return tiempoTransito;
	}
	
        /**
         * Este metodo calcula el tiempo de transito sumandole la diferencia entre
           el tiempo actual y el tiempo en el que arribo.
         * @param tiempoArribo es el tiempo que arriba el item 
	 * @param tiempoActual es el tiempo actual.
	 */
	public static void setTiempoTransito(int tiempoActual, int tiempoArribo) {
			tiempoTransito += (tiempoActual - tiempoArribo);
	}
	
	/**
	 * @return Returns the numero.
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * @param numero The numero to set.
	 */
	public void setNumero(int numero) {
		this.numero = numero;
	}

	/**
	 * @return Returns the tiempoArribo.
	 */
	public int getTiempoArribo() {
		return this.tiempoArribo;
	}

	/**
	 * @param tiempoArribo The tiempoArribo to set.
	 */
	public void setTiempoArribo(int tiempoArribo) {
		this.tiempoArribo=tiempoArribo;
	}

	/**
	 * @return Returns the tiempoDuracionServicio.
	 */
	public int getTiempoDuracionServicio() {
		return tiempoDuracionServicio;
	}

	/**
	 * @param tiempoDuracionServicio The tiempoDuracionServicio to set.
	 */
	public void setTiempoDuracionServicio(int tiempoDuracionServicio) {
		this.tiempoDuracionServicio = tiempoDuracionServicio;
	}
	
	public String toString() {
		String aux="";
		switch(this.tiempoDuracionServicio) {
			case 0: aux = "No calculado"; break;
			default: aux = Integer.toString(this.tiempoDuracionServicio);
		}
		return "Item n�: " + this.numero + "\nTiempo de Arribo: " + this.tiempoArribo + "\nDuracion del servicio: " + aux;
	}
}