package Eventos;

public class Estadisticas {
	/** Este metodo analiza la simulacion mostrando:
            El tiempo medio de espera por el servicio de descenso, 
            Porcentaje de tiempo ocioso de la pista y
            el tiempo medio de tránsito.
        * @param tiempoEsperaCola es el tiempo que tardo un item en la cola 
        * @param tiempoTransito es el tiempo que tardo el item en todo el servidor
        * @param tiempoOcioso es el tiempo de inactividad del servidor
        * @param tiempoFinSimulacion es el tiempo en el que finalizo la simulacion
        * @param cantidadItems es la cantidad de items involucrados en la simulacion
        */ 
	public static String calcularEstadisticas(float tiempoEsperaCola, float tiempoTransito, float tiempoOcioso, float tiempoFinSimulacion, int cantidadItems){
	    if(cantidadItems == 0){
                cantidadItems++;
                tiempoFinSimulacion++;
            }   
            return " Tiempo medio de espera de descenso: " + (tiempoEsperaCola / cantidadItems) + " Minutos" + "\nPorcentaje de tiempo ocioso de la pista: " +
	    		   ((tiempoOcioso / tiempoFinSimulacion) * 100) + "%" +
	              "\nTiempo medio de transito: " + (float)(tiempoTransito / cantidadItems) + " Minutos";
	}
}
