/*
Gurruchaga Luciano tulitee10@gmail.com
Rosas Amaya Francisco fran_281197@hotmail.com
*/

package Eventos;

public class Principal {
			
	public static void main(String[] args){
		Evento ultimo, actual;
		Queue queue = new Queue();
		Servidor servidor = new Servidor();
                /*
                Los tipos de los eventos estan seteados por defecto en cada constructor. 
                Asi tambien como los tiempos del item son calculados automaticamente en cada evento.
                */
		ultimo = new EventoFinSimulacion(10800, new Item());
		actual = new EventoArribo(0,new Item());
                /*
                El orden de la fel esta ordenado por tiempo en primer lugar y en caso de ser iguales,
                ordena por tipo de evento. Esto se ve en la clase evento.
                */
                Fel.getFel().insertarFel(ultimo);
		Fel.getFel().insertarFel(actual);
                while(actual.getTipo() != 2) {
               
			actual=Fel.getFel().suprimirFel();
			actual.planificarEvento(servidor, queue);
			System.out.println("------Evento actual procesado----\n" + actual.toString());
		//	System.out.println("-------FEL Actual-------");
		//	Fel.getFel().mostrarFel();
		}
                /**
                 * Nosotros consideramos que al terminar la simulacion, si hay un ultimo Item esperando a ser antendido o siendo atendido
                 * Como nunca fue atendido por el servidor o el servidor nunca termino de atenderlo.
                 * Nunca salio del sistema durante el tiempo de simulacion.
                 * Por lo tanto no consideramos que tenga un tiempo de transito al no conocerlo.
                 */
                System.out.println(Estadisticas.calcularEstadisticas(Item.getTiempoEsperaCola(), Item.getTiempoTransito(), servidor.getTiempoOcioso(), ultimo.getTiempo(), Item.getCantidadItems()));
	}
}
