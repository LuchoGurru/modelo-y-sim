package Eventos;
import java.util.LinkedList;
import Eventos.Item;

public class Queue {
	private int cantidadItems;
	private LinkedList<Item> cola;
	
	public Queue(){
		cola = new LinkedList<Item>();
		cantidadItems=0;
	}
	public void insertarCola(Item item){
		cola.add(item);
		cantidadItems++;
	}
	
	public Item suprimirCola(){
		if(this.HayCola()) {
			cantidadItems--;
			return cola.remove();
		}
		return null;
	}
	
	public boolean HayCola(){
		return cantidadItems > 0;
	}
}
