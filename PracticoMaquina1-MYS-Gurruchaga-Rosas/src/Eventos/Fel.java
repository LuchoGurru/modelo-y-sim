package Eventos;
import java.util.Collections;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import Eventos.*;

public class Fel {
	private static Fel fel;
	private LinkedList<Evento> lista;
        
	private Fel(){
            lista=new LinkedList<Evento>();	
	}
	//Singleton
	public static Fel getFel(){
		if(fel==null)
			fel=new Fel();
		return(fel);
	}
	
	public void insertarFel(Evento e){
		lista.add(e);
		Collections.sort(lista);
	}
/**
 * suprime un evento de la fel 
 * @return retorna nulo si no hay elementos en la fel
 */
	public Evento suprimirFel() {
		try {
			return lista.pop();
		}
		catch(NoSuchElementException e) {
			System.out.println("\nLa FEL se encuentra vacia");
			return null;
		}
	}
	
	public void mostrarFel(){
		for(Evento e : this.lista) {
			System.out.println(e.toString());
		}
	}
	
	/**
	 * @return Returns the lista.
	 */
	public LinkedList<Evento> getLista() {
		return lista;
	}
	
}
