package Eventos;
import java.util.Random;


public class GeneradorTiempos {
    private static Random random;
    static{
        random=new Random(System.currentTimeMillis());
    }
    /**
     * Genera un numero entero aleatorio [4,6]
     * 4 y 5 con una probabilidad del 30%
     * 6 con probabilidad del 40%
     * @return int
     */
    public static int getTiempoEntreArribos(){
    	int aux = random.nextInt(10);
        if(aux < 3) {
        	return 4;
        }
        if(aux < 6) {
        	return 5;
        }
        return 6;
    }
    /**
     * Genera un numero entero aleatorio [3,5]
     * 3 y 5 con una probabilidad del 30%
     * 4 con probabilidad del 40%
     * @return int
     */
    public static int getTiempoDuracionServicio(){
    	int aux = random.nextInt(10);
    	if(aux < 3) {
        	return 3;
        }
        if(aux < 7) {
        	return 4;
        }
        return 5;
    }
}
