package Eventos;

public abstract class Evento implements Comparable<Evento> {
	private int tipo;//0: Arribo - 1:Fin de Servicio - 2: Fin de Simulacion
	private int tiempo; //Tiempo ocurrencia salida/arribo
	private Item item;
	
	public Evento(int tipo,int tiempo,Item item){
		this.setTipo(tipo);
		this.setTiempo(tiempo);
		this.item=item;
	}
    public Item getItem() {
        return item;
    }
    public void setItem(Item item) {
        this.item=item;
    }
    public int getTipo() {
    	return tipo;
    }
    public void setTipo(int tipo) {
    	this.tipo = tipo;
    }
    public int getTiempo() {
    	return tiempo;
    }
    public void setTiempo(int tiempo) {
   		this.tiempo = tiempo;
   	}

/**
 * Ordena los eventos por tiempo desde el mas proximo al menos proximo, si tienen el mismo tiempo, prioriza segun
 * el tipo de evento, el mas prioritario es el fin de simulacion y el de menos prioridad es el de arribo.
 * siempre que no se trate del evento de fin de simulacion.
 * @param e es el evento a comparar
 * @return 1 si el evento del parametro tiene un tiempo menor,-1 si es mayor. Si son iguales, retorna 1 si el tipo del parametro es menor, sino retorna 0. 
 */
    public int compareTo(Evento e) {
   		int aux = this.tiempo - e.getTiempo();
   		if(aux > 0)
   			return 1;
   		if(aux < 0)
   			return -1;
   		if(this.tipo > e.getTipo())
   			return 1;
   		return 0;
   	}
   	 
   	public String toString() {
   		String aux="";
   		if(this.tipo==0) {
   			aux = "Evento Arribo";
   		}
   		if(this.tipo==1) {
   			aux = "Evento Salida";
   		}
   		if(this.tipo==2) {
   			aux = "Evento Fin Simulacion";
   		}
   		return "\n"+aux + "\nTiempo: " + this.tiempo + "\n" + this.item.toString()+"\n";
   	}

/**
 *  Implementa la planiificaci�n de eventos.
 */
	public abstract void planificarEvento(Servidor servidor,Queue queue);

}
