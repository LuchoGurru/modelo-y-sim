package Eventos;

public class EventoFinSimulacion extends Evento {
	/**
         * Crea un evento de tipo 2 el cual finalizara la simulacion
         * @param tiempo tiempo del evento
         * @param ultimo  item del evento
         */
	public EventoFinSimulacion(int tiempo, Item ultimo){
		super(2,tiempo, ultimo);
		ultimo.setTiempoArribo(-1);
		ultimo.setTiempoDuracionServicio(-1);
		
	}
        /**
         * Dado que se puede dar el caso de que la simulacion termine cuando el servidor esta ocioso. 
         * Contamos el tiempo de ocio del servidor hasta que termine la simulacion.
         * @param servidor
         * @param queue 
         */
	public void planificarEvento(Servidor servidor,Queue queue){
            if(!servidor.isEstado()){
                servidor.setTiempoOcioso(this.getTiempo() - servidor.getTiempoInicioOcio());
            }
                
	}
}
